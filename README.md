![SensorSpace Logo](https://sensorspace.tech/images/logo1_vertical.png)

# Flathead Lake Biological Station - Tutorial #1 on using the latest Big Data tools for ecological research.
In this example we will be setting up a web server from scratch, which will be used to plot sensor data in real-time. The server will retrieve data from The Things Network using Node-RED, store the data using InfluxDB, and plot the data using Grafana. The web page with the live data will be served by Apache.

## 1.) Creating Your Web Server
The first thing we need to do is choose a company to host your web server. A popular choice is DigitalOcean - this is the company we will be using for this tutorial. Create an account: https://www.digitalocean.com/

Once you've created an account, click on **Create->Droplets** at the top right of the DigitalOcean website. DigitalOcean calls their servers "Droplets" (it's just a marketing thing...).

![enter image description here](https://s22.postimg.cc/6w25kc55d/image.png)
